Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xrt
Upstream-Contact: Sonal Santan <sonal.santan@xilinx.com>
Source: https://github.com/Xilinx/XRT.git
Files-Excluded:
 src/runtime_src/core/edge/user/test/usertest

Files: *
Copyright: 2015-2021, Xilinx, Inc
License: Apache-2.0

Files: src/runtime_src/core/common/drv/*
 src/runtime_src/core/edge/drm/zocl/*
 src/runtime_src/core/edge/include/zynq_ioctl.h
 src/runtime_src/core/pcie/driver/aws/kernel/mgmt/mgmt-sysfs.c
 src/runtime_src/core/pcie/driver/linux/*
 src/runtime_src/core/pcie/driver/linux/xocl/*
 src/runtime_src/core/pcie/driver/linux/xocl/subdev/qdma4.c
Copyright: 2017 - 2020 Xilinx, Inc.
License: GPL-2

Files: src/runtime_src/core/pcie/driver/aws/kernel/mgmt/mgmt-bit.c
 src/runtime_src/core/pcie/driver/aws/kernel/mgmt/mgmt-core.c
 src/runtime_src/core/pcie/driver/aws/kernel/mgmt/mgmt-cw.c
 src/runtime_src/core/pcie/driver/aws/kernel/mgmt/mgmt-firewall.c
 src/runtime_src/core/pcie/driver/aws/kernel/mgmt/mgmt-thread.c
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libxdma.c
 src/runtime_src/core/pcie/driver/linux/xocl/mgmtpf/mgmt-core.c
 src/runtime_src/core/pcie/driver/linux/xocl/mgmtpf/mgmt-core.h
 src/runtime_src/core/pcie/driver/linux/xocl/mgmtpf/mgmt-cw.c
 src/runtime_src/core/pcie/driver/linux/xocl/mgmtpf/mgmt-ioctl.c
 src/runtime_src/core/pcie/driver/linux/xocl/mgmtpf/mgmt-utils.c
 src/runtime_src/core/pcie/driver/linux/xocl/subdev/firewall.c
 src/runtime_src/core/pcie/driver/linux/xocl/subdev/icap.c
 src/runtime_src/core/pcie/driver/linux/xocl/xocl_thread.c
 src/runtime_src/core/pcie/driver/aws/kernel/mgmt/mgmt-bit.h
 src/runtime_src/core/pcie/driver/aws/kernel/mgmt/mgmt-core.h
Copyright: 2015-2020, Xilinx, Inc.
License: GPL-2+

Files: src/runtime_src/core/include/ert.h
 src/runtime_src/core/include/ert_fa.h
 src/runtime_src/core/include/xclfeatures.h
 src/runtime_src/core/include/xrt_error_code.h
 src/runtime_src/core/pcie/driver/linux/include/cdev_ctrl.h
 src/runtime_src/core/pcie/driver/linux/include/mgmt-ioctl.h
 src/runtime_src/core/pcie/driver/linux/include/qdma_ioctl.h
 src/runtime_src/core/pcie/driver/linux/xocl/devices.h
 src/runtime_src/tools/scripts/header.lic
Copyright: 2015-2020, Xilinx Inc
License: Apache-2.0 and/or GPL-2+

Files: src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/fdt.c
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/fdt_empty_tree.c
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/fdt_ro.c
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/fdt_rw.c
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/fdt_strerror.c
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/fdt_sw.c
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/fdt_wip.c
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/libfdt.h
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/libfdt_internal.h
Copyright: 2006, David Gibson, IBM Corporation.
  2012, David Gibson, IBM Corporation.
License: BSD-2-clause and/or GPL-2+

Files: src/runtime_src/core/common/api/aie/xrt_graph.cpp
 src/runtime_src/core/edge/user/aie/AIEResources.cpp
 src/runtime_src/core/edge/user/aie/AIEResources.h
 src/runtime_src/core/include/xcl_graph.h
 src/runtime_src/core/pcie/driver/linux/include/libxdma_api.h
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libxdma_api.h
Copyright: 2016-2020, Xilinx, Inc
License: Apache-2.0 and/or GPL-2

Files: src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/fdt.h
 src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/libfdt_env.h
Copyright: 2006, David Gibson, IBM Corporation.
  2012, Kim Phillips, Freescale Semiconductor.
License: BSD-2-clause and/or GPL-2+

Files: src/include/1_2/CL/cl_ext.h
 src/include/1_2/CL/cl_ext_xilinx.h
Copyright: 2008-2015, The Khronos Group Inc.
License: Khronos

Files: src/runtime_src/core/pcie/windows/shim.cpp
Copyright: 2019, Samsung Semiconductor, Inc
  2019, Xilinx, Inc
License: Apache-2.0

Files: src/runtime_src/xocl/api/icd/windows/icd_dispatch.h
Copyright: 2016-2019, The Khronos Group Inc.
License: Apache-2.0

Files: src/runtime_src/core/pcie/emulation/hw_em/zynqu/pllauncher_defines.h
Copyright: 2016-2017, Xilinx, Inc
License: Apache-2.0 and/or Expat

Files: src/runtime_src/core/include/xclerr.h
       src/runtime_src/core/pcie/driver/linux/include/xocl_ioctl.h
Copyright: 2017, Xilinx, Inc.
License: Apache-2.0 and/or GPL-2+

Files: src/runtime_src/core/include/xclbin.h
Copyright: 2015-2020, Xilinx Inc
License: Apache-2.0 and/or GPL-3+

Files: src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/fdt_addresses.c
Copyright: 2014, David Gibson <david@gibson.dropbear.id.au>
  2018, embedded brains GmbH
License: BSD-2-clause and/or GPL-2+

Files: src/runtime_src/core/pcie/driver/linux/xocl/lib/libfdt/fdt_overlay.c
Copyright: 2016, Free Electrons
  2016, NextThing Co.
License: BSD-2-clause and/or GPL-2+

Files: tests/xrt/soft_kernel_hello/dummy_plugin_enc.c
Copyright: 2018, Xilinx, Inc.
License: BSD-3-clause

Files: build/checkpatch/spelling.txt
Copyright: Xilinx, Inc.
License: GPL-2

Files: src/runtime_src/core/pcie/driver/linux/xocl/subdev/ulite.c
Copyright: 2006, Peter Korsgaard <jacmet@sunsite.dk>
  2007, Secret Lab Technologies Ltd.
  2020, Chien-Wei Lan <chienwei@xilinx.com>
License: GPL-2

Files: src/runtime_src/core/pcie/driver/linux/xocl/subdev/xiic.c
Copyright: 2009-2010, Intel Corporation
  2017, Xilinx, Inc.
License: GPL-2+

Files: src/runtime_src/core/pcie/driver/windows/include/XoclUser_INTF.h
Copyright: 2019, OSR Open Systems Resources, Inc.
  2019, Xilinx, Inc.
License: Apache-2.0

Files: src/runtime_src/xocl/api/khronos/check_copy_overlap.cpp
Copyright: 2011, The Khronos Group Inc.
License: Khronos

Files: debian/*
Copyright: 2021, Punit Agrawal <punit@debian.org>
           2021,2022, Nobuhiro Iwamatsu <iwamatsu@debian.org>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache License version 2 can
 be found in /usr/share/common-licenses/Apache-2.0.

License: Khronos
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and/or associated documentation files (the
 "Materials"), to deal in the Materials without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Materials, and to
 permit persons to whom the Materials are furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Materials.
 .
 THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.

License: GPL-2
 On Debian systems the full text of the GPL-2 can be found in
  /usr/share/common-licenses/GPL-2

License: GPL-2+
 On Debian systems the full text of the GPL-2 can be found in
  /usr/share/common-licenses/GPL-2

License: GPL-3+
 On Debian systems the full text of the GPL-3 can be found in
  /usr/share/common-licenses/GPL-3

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1) Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3) Neither the name of the ORGANIZATION nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
